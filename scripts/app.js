var fakeArticle = {
	article_id: "EN_000",
	headline: "Lorem ipsum dolor sit amet",
	body: [
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vulputate ante vel neque eleifend mattis. Curabitur vehicula arcu ac ultricies rhoncus. Integer rhoncus rutrum nulla, nec vulputate ligula lobortis at. Aliquam quis aliquam arcu. Maecenas nec nunc vitae enim sagittis tempor a vitae tellus. Pellentesque tristique convallis diam, vitae ullamcorper tellus malesuada et. Sed consectetur nisl vel elementum aliquam. Duis lacinia sapien vel leo tempus, eget ornare nisl consectetur. Suspendisse dictum iaculis velit, vel pellentesque ipsum mollis ultrices. Nullam nec venenatis sem. ",
		"Donec in erat rhoncus, malesuada nisi vitae, hendrerit odio. Sed vulputate vehicula lectus, nec fringilla enim facilisis quis. Duis consectetur, arcu eu tincidunt ultricies, turpis elit blandit turpis, ac interdum dolor quam eget ipsum. Etiam ullamcorper ut lorem in ullamcorper. Curabitur et sagittis velit. Nam justo erat, pellentesque vel turpis id, egestas tincidunt elit. Aliquam erat volutpat. Vivamus consectetur quam eu volutpat tempor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam erat volutpat. Maecenas sodales sapien et nisl consectetur iaculis. Pellentesque malesuada eget tellus ut finibus.",
		"Fusce fermentum pretium nisi ac facilisis. Ut placerat massa et turpis tristique malesuada. Nam volutpat, magna ac tristique molestie, nibh est elementum diam, a hendrerit leo ipsum id eros. Duis eget metus vitae felis fringilla scelerisque. Proin condimentum nibh ligula, at feugiat odio posuere quis. Sed gravida massa nulla, et ornare augue aliquam vel. Sed ac ex lobortis, blandit tortor nec, pharetra orci. Phasellus sit amet ex eu mi iaculis interdum. Nulla eget pharetra neque.",
		"Nulla vitae enim tempor, consequat tellus id, finibus ex. Pellentesque vel tortor sed lectus feugiat sodales ut ac dui. Phasellus at tellus et ligula consequat ultricies varius et sapien. Aenean scelerisque cursus pretium. Aliquam elementum mi sit amet eros mollis, nec aliquam justo dignissim. Phasellus pulvinar tempor diam, non posuere nunc facilisis ut. Duis luctus tortor quis aliquet congue. ",
		"Nulla dignissim, massa eget convallis tempus, ante enim fermentum tellus, a elementum nulla ipsum ultrices elit. Proin scelerisque eleifend feugiat. Duis neque tortor, cursus ac ante sed, tempus hendrerit eros. Suspendisse nisl magna, finibus posuere finibus nec, tincidunt et massa. Morbi dapibus bibendum risus sit amet mollis. Donec interdum, ex vulputate consectetur condimentum, augue nulla iaculis mi, at tincidunt neque mi ut tellus. Maecenas tincidunt rutru"
	]
};

var initialArticleSources = [{
	name: "The Hindu",
	url: "http://www.thehindu.com/news/national/",
	category: "National"
}];

var initialUserPreferences = {
	user_id: "000",
	article_sources: initialArticleSources,
	font_size: 1
};

var DB_VERSION = 1;
const DB_READWRITE = "readwrite";
const DB_READONLY = "readonly";

function renderArticle(article) {
	let articleBlock = document.createElement("article");
	let headline = document.createElement("h3");
	headline.textContent = article.headline;
	articleBlock.appendChild(headline);
	article.body.forEach((para) => {
		let p = document.createElement("p");
		p.textContent = para;
		articleBlock.appendChild(p);
	});
	return articleBlock;
}

function refreshArticles(dbHandle) {
	if(dbHandle !== undefined || dbHandle !== null) {
		let tpromise = new Promise((resolve, reject) => {
			let transaction = dbHandle.transaction(["Articles"], DB_READONLY);
			let articles = [];
			transaction.onerror = function() {
				reject(new Error("Transaction failed!"));
			};
			transaction.oncomplete = function() {
				resolve(articles);
			};
			let store = transaction.objectStore("Articles");
			let cursorRequest = store.openCursor();
			cursorRequest.onsuccess = function() {
				let cursor = cursorRequest.result;
				while(cursor) {
					articles.push(cursor.value);
					cursor.continue(); // cursor iterates past the end point and throws error, need to add check here
				}
			};
			cursorRequest.onerror = function() {
				reject(new Error("Unable to get object store cursor"));
			};
		});
		return tpromise;
	}
}

function initializeIndexedDB(version = 1) {
	if(window.indexedDB !== undefined) {
		let dbPromise = new Promise((resolve, reject) => {
			let dbRequest = window.indexedDB.open("News180", version);
			dbRequest.onerror = function() {
				reject(new Error("Could not initialize indexed db"));
			};
			dbRequest.onsuccess = function() {
				console.log("indexed DB initialized");
				resolve(dbRequest.result);
			};
			dbRequest.onversionchange = function() {
				console.log("Version of the indexed db has changed need to reopen the app in another browser");
				reject(new Error("Version change has been done need to refresh app"));
			};
			dbRequest.onupgradeneeded = function() {
				console.log("Creating object stores...");
				let dbHandle = dbRequest.result;

				dbHandle.onerror = function() {
					console.log("Error creating Object stores");
					reject(new Error("Error creating object stores"));
				};

				dbHandle.onabort = function() {
					console.log("Object store creation was aborted");
					reject(new Error("Object store creation was aborted"));
				};

				let articleStore = dbHandle.createObjectStore("Articles", {keyPath: "article_id"});
				articleStore.createIndex("headline", "headline", {unique: false});
				articleStore.createIndex("body", "body", {unique: false});

				let userPreferenceStore = dbHandle.createObjectStore("UserPreferences", {keyPath: "user_id"});
				userPreferenceStore.createIndex("article_sources", "article_sources", {unique: false});
				userPreferenceStore.createIndex("font_size", "font_size", {unique: false});
			};
		});
		return dbPromise;
	} else {
		throw new Error("Indexed DB is not available, cannot cache preferences");
	}
}

function addNewArticle(dbHandle, article) {
	if((dbHandle !== undefined || dbHandle !== null) && (article !== undefined || article !== null)) {
		let tpromise = new Promise((resolve, reject) => {
			let transaction = dbHandle.transaction(["Articles"], DB_READWRITE);
			transaction.onerror = function() {
				reject(new Error("Transaction failed!"));
			};
			transaction.oncomplete = function() {
				resolve(article);
			};
			let store = transaction.objectStore("Articles");
			store.add(article);
		});
		return tpromise;
	}
}

function addNewArticleSource(source, callback) {
	callback();
}

window.onload = function() {
	console.log("Loaded Progressive web app!");
	initializeIndexedDB(DB_VERSION)
		.then((dbHandle) => {
			let refreshButton = document.querySelector("button#refresh");
			let addCategoryButton = document.querySelector("button#add-category");
			let hideLoading = function() {
				document.querySelector("div#loader").className = "hidden";
			};
			let showLoading = function() {
				document.querySelector("div#loader").className = "visible";
			};
			let onArticleAdd = function(article) {
				hideLoading();
				document.querySelector("div#article-container").appendChild(article);
			};
			let onRefresh = function(dbHandle) {
				let refresh = refreshArticles(dbHandle).then((result) => {
					let container = document.querySelector("div#article-container");
					container.childNodes.forEach((e) => container.removeChild(e));
					showLoading();
					console.log(result);
				}).catch((e) => {throw e});
			};
			refreshButton.addEventListener("click", onRefresh.bind(this, dbHandle));
			addCategoryButton.addEventListener("click", addNewArticleSource.bind(this, initialArticleSources));
			addNewArticle(dbHandle, fakeArticle);
			console.log(dbHandle);
		}).catch((e) => {throw e});
};
