# Progressive Web Applications
------------------------------

This is my first go at progressive web applications. I am trying to build a small
news reading application which can be added with news sources and viewed on a
browser, mobile and tablet.

The main things this demo will teach me are as follows

* Usage of **IndexedDB** (Refresher since I have used it before)
* Usage of **Progressive Web App principles**
* Usage of **Service Workers** for caching and loading application data

The tutorial I am following is [here](https://codelabs.developers.google.com/codelabs/your-first-pwapp/#1)
